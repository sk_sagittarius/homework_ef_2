﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkEF2
{
    public class EditProduct
    {
        public void Edit()
        {
            using (var context = new StorageContext())
            {
                Storage storage = new Storage();
                var storageList = context.Storages.ToList();
                Console.WriteLine("{0}   \t{1}   \t{2}\n", "N", "Product", "Quantity");
                for (int i = 0; i < storageList.Count; i++)
                {
                    Console.WriteLine("{0} . \t{1} - \t{2}", i + 1, storageList.ElementAt(i).Name, storageList.ElementAt(i).Quantity);
                }
                Console.WriteLine("Edit Name or Quantity?\n");
                try
                {
                    var editOrQuantityKey = Console.ReadLine().ToLower();
                    if (editOrQuantityKey == "name")
                    {
                        Console.WriteLine("Choose number of product to edit name");
                        var editKey = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter new name");
                        var newName = Console.ReadLine();
                        storageList.ElementAt(editKey - 1).Name = newName;
                    }
                    else if (editOrQuantityKey == "quantity")
                    {
                        Console.WriteLine("Choose number of product to edit quantity");
                        var editKey = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter new quantity");
                        var newQuantity = int.Parse(Console.ReadLine());
                        //context.Storages.Find(storageList.ElementAt(editKey - 1)).Quantity = newQuantity;
                        storageList.ElementAt(editKey - 1).Quantity = newQuantity;
                    }
                    else
                    {
                        Console.WriteLine("Error!");
                    }

                    context.SaveChanges();
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Wrong format");
                }
                catch(Exception exception)
                {
                    Console.WriteLine("Somethin is wrong!");
                }

            }
        }
    }
}
