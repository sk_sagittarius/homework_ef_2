﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkEF2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choose some function");
            Console.WriteLine("{0}   \t{1}\n", "N", "Function");
            Console.WriteLine("{0} . \t{1}", "1", "Show all products");
            Console.WriteLine("{0} . \t{1}", "2", "Add new product");
            Console.WriteLine("{0} . \t{1}", "3", "Delete product");
            Console.WriteLine("{0} . \t{1}", "4", "Edit product");



            var key = int.Parse(Console.ReadLine());
            Console.Clear();
            switch (key)
            {
                case 1:
                    ShowAllProducts showAllProducts = new ShowAllProducts();
                    showAllProducts.Show();
                    break;
                case 2:
                    AddProducts addProducts = new AddProducts();
                    addProducts.Add();
                    break;
                case 3:
                    DeleteProduct deleteProduct = new DeleteProduct();
                    deleteProduct.Delete();
                    break;
                case 4:
                    EditProduct editProduct = new EditProduct();
                    editProduct.Edit();
                    break;
                default: Console.WriteLine("Wrong number");
                    break;
            }

            //AddProducts addProducts = new AddProducts();
            //addProducts.Add();

            //DeleteProduct deleteProduct = new DeleteProduct();
            //deleteProduct.Delete();

            //EditProduct editProduct = new EditProduct();
            //editProduct.Edit();

            //ShowAllProducts showAllProducts = new ShowAllProducts();
            //showAllProducts.Show();

            Console.Read();
        }
    }
}
