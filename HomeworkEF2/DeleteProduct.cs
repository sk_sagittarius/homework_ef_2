﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkEF2
{
    public class DeleteProduct
    {
        public void Delete()
        {
            using (var context = new StorageContext())
            {
                Storage storage = new Storage();
                var storageList = context.Storages.ToList();
                Console.WriteLine("{0}   \t{1}   \t{2}\n", "N", "Product", "Quantity");
                for (int i = 0; i < storageList.Count; i++)
                {
                    Console.WriteLine("{0} . \t{1} - \t{2}", i+1, storageList.ElementAt(i).Name, storageList.ElementAt(i).Quantity);
                }

                Console.WriteLine("Choose number of product to delete");
                var deleteKey = int.Parse(Console.ReadLine());
                context.Storages.Remove(storageList.ElementAt(deleteKey-1));
                context.SaveChanges();
            }
        }
    }
}
