﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkEF2
{
    public class AddProducts
    {
        public void Add()
        {
            using (var context = new StorageContext())
            {
                Storage storage = new Storage();
                Console.WriteLine("Add products");
                Console.WriteLine("Product name");
                storage.Name = Console.ReadLine();
                if (context.Storages.Any(stor => stor.Name == storage.Name))
                {
                    Console.WriteLine("Product is exist!");
                }
                else
                {
                    Console.WriteLine("Product quantity");
                    storage.Quantity = int.Parse(Console.ReadLine());

                    context.Storages.Add(storage);
                    context.SaveChanges();
                }
            }
        }
    }
}
